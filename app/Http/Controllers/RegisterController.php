<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller {
    //
    public function form() {
        return view('register');
    }

    public function welcome(Request $request) {
        $namaDepan = $request->input('nama_depan');
        $namaBelakang = $request->input('nama_belakang');
        // return "Nama Depan : ".$namaDepan.", Nama Belakang : ".$namaBelakang;
        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }
}
