<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    <form action="/formulir/welcome" method=POST>
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <label>First name:</label> <br/><br/>
        <input type="text" name="nama_depan"> <br/><br/>
      <label>Last name:</label> <br/><br/>
        <input type="text" name="nama_belakang"> <br/><br/>
      <label>Gender:</label> <br/><br/>
        <input type="radio" name="jenis_kelamin"> Male <br/>
        <input type="radio" name="jenis_kelamin"> Female <br/>
        <input type="radio" name="jenis_kelamin"> Other <br/> <br/>
      <label>Nationality:</label> <br/><br/>
        <select name="Nationality">
          <option value="Indonesian">Indonesian</option>
          <option value="English">Inggris</option>
          <option value="Malaysian">Malaysian</option>
        </select> <br/><br/>
      <label>Language Spoken:</label> <br/><br/>
        <input type="checkbox" name="language"> Bahasa Indonesia <br/>
        <input type="checkbox"name="language"> Bahasa Inggris <br/>
        <input type="checkbox" name="language"> Other <br/> <br/>
      <label>Bio:</label> <br/><br/>
      <textarea name="bio" cols="40" rows="10" placeholder="isikan bio disini"></textarea> <br/>
      <button type="submit">Sign Up</button>
    </form>
    
</body>
</html>